package service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Predicate;

import parser.Parser;
import parser.model.Sentence;

public class SentenceService {
    public static int parseAndPrintCSV(Scanner sc) {
        int longestSentenceCount = 0;

        StringBuilder csvStringBuilder = new StringBuilder();
        int i = 0;

        while (sc.useDelimiter("[./?/!]").hasNext()) {
            Sentence sentence = Sentence.buildSentence(sc.next());
            List<String> wordsInSentence = sentence.getWordsInSentence();
            if (wordsInSentence.isEmpty()) {
                continue;
            }
            i++;
            wordsInSentence = sortLexicographically(wordsInSentence);

            longestSentenceCount = wordsInSentence.size() > longestSentenceCount ? wordsInSentence.size()
                    : longestSentenceCount;
        }
        csvStringBuilder.setLength(0);
        StringBuilder headerStringBuilder = new StringBuilder();
        for (int j = 1; j <= longestSentenceCount; j++) {
            headerStringBuilder.append(", Word " + j);
        }
        System.out.println(headerStringBuilder.append(csvStringBuilder));
        headerStringBuilder.setLength(0);
        Scanner scanAgainBuildBody = Parser.getScannerForFile(Parser.getInputFile());
        i = 0;

        while (scanAgainBuildBody.useDelimiter("[./?/!]").hasNext()) {

            Sentence sentence = Sentence.buildSentence(scanAgainBuildBody.next());

            List<String> wordsInSentence = sentence.getWordsInSentence();
            if (wordsInSentence.isEmpty()) {
                continue;
            }
            i++;
            wordsInSentence = sortLexicographically(wordsInSentence);

            csvStringBuilder.append("Sentence " + i);
            String wordsForSentence = sentence.printAsCSV(wordsInSentence);
            csvStringBuilder.append(wordsForSentence);
            System.out.println(csvStringBuilder.toString());
            csvStringBuilder.setLength(0);

        }
        return longestSentenceCount;
    }

    public static void parseAndPrintXML(Scanner sc) {
        StringBuilder sb = new StringBuilder();
        String startXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<text>";
        System.out.print(startXML);
        while (sc.useDelimiter("[./?/!]").hasNext()) {
            Sentence sentence = Sentence.buildSentence(sc.next());

            List<String> wordsInSentence = sentence.getWordsInSentence();
            if (wordsInSentence.isEmpty()) {
                continue;
            }
            wordsInSentence = SentenceService.sortLexicographically(wordsInSentence);

            String wordsForSentence = sentence.printAsXMLSentenceWithWordsElements(wordsInSentence);
            sb.append(wordsForSentence);
            System.out.print(sb.toString());
            sb.setLength(0);
        }
        sb.append("\n</text>");
        System.out.println(sb.toString());
    }

    public static List<String> sortLexicographically(List<String> wordsInSentence) {
        Collections.sort(wordsInSentence, new Comparator<String>() {
            public int compare(String a, String b) {// if both are numbers
                if (a.matches("\\d+") && b.matches("\\d+")) {
                    return new Integer(a.toLowerCase()) - new Integer(b.toLowerCase());
                }// else, compare normally.
                return a.toLowerCase().compareTo(b.toLowerCase());
            }
        });
        return wordsInSentence;
    }

}
