package parser.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class Sentence {

    String       sentence;
    List<String> wordsInSentence = new ArrayList<String>();

    public String printAsXMLSentenceWithWordsElements(List<String> words) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n    <sentence>\n");
        for (String word : words) {
            sb.append("        <word>");
            sb.append(StringUtils.deleteWhitespace(word));
            sb.append("</word>\n");
        }
        sb.append("    </sentence>");
        return sb.toString();
    }

    public String printAsCSV(List<String> words) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0, size = words.size(); i < size; i++) {
            sb.append(", " + StringUtils.deleteWhitespace(words.get(i)));
        }
        return sb.toString();
    }

    private static Predicate<String> isWordNotEmpty() {
        return word -> !word.replaceAll("[ .,\\s\t\n\r]", "").isEmpty();
    }

    public static Sentence buildSentence(String line) {
        Sentence sentence = new Sentence();
        sentence.setSentence(line);
        sentence.setWordsInSentence(getStringAsListOfWords(line));
        return sentence;
    }

    public static List<String> getStringAsListOfWords(String line) {
        List<String> wordsInSentence = Arrays.asList(line.split(" "));
        wordsInSentence = wordsInSentence.stream().filter(isWordNotEmpty()).collect(Collectors.toList());
        return wordsInSentence;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<String> getWordsInSentence() {
        return wordsInSentence;
    }

    public void setWordsInSentence(List<String> wordsInSentence) {
        this.wordsInSentence = wordsInSentence;
    }

}
