package parser;

import java.util.Optional;
import java.util.Scanner;

import org.apache.log4j.Logger;

import service.SentenceService;

public class Parser {

    private final static Logger LOGGER    = Logger.getLogger(Parser.class.getName());
    public static String        inputFile = "large.in";

    public static void main(String[] args) {
        // System.out.println(Runtime.getRuntime().maxMemory() / 1000000 +
        // " MB");

        Scanner sc = getScannerForFile(inputFile);
        LOGGER.info("We are parsing the data for file " + inputFile);
        SentenceService.parseAndPrintXML(sc);
        SentenceService.parseAndPrintCSV(sc);

    }

    /**
     * Get scanner object for given file from resources src/main/resources
     * folder.
     * 
     * @param inputFile name
     * @return Scanner
     */
    public static Scanner getScannerForFile(String inputFile) {
        Optional<Scanner> sc = Optional.ofNullable(new Scanner(Parser.class.getClassLoader().getResourceAsStream(
                inputFile), "UTF-8"));
        if (!sc.isPresent()) {
            throw new RuntimeException("No file named " + Parser.getInputFile() + " present.");
        }
        return sc.get();
    }

    /**
     * Get scanner object for given file from resources src/main/resources
     * folder.
     * 
     * @return Scanner for default file under the field inputField
     */
    public static Scanner getScannerForFile() {
        Optional<Scanner> sc = Optional.ofNullable(new Scanner(Parser.class.getClassLoader().getResourceAsStream(
                inputFile), "UTF-8"));
        if (!sc.isPresent()) {
            throw new RuntimeException("No file named " + Parser.getInputFile() + " present.");
        }
        return sc.get();
    }

    public static String getInputFile() {
        return inputFile;
    }

    public static void setInputFile(String inputFile) {
        Parser.inputFile = inputFile;
    }

}
