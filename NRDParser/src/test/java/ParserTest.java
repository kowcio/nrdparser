import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import junit.framework.Assert;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import parser.Parser;
import parser.model.Sentence;
import service.SentenceService;

@Test
public class ParserTest {

    private final static Logger LOGGER = Logger.getLogger(ParserTest.class.getName());

    public void parseCSVTest() {

        String out = "<sentence><word>a</word><word>had</word>"
                + "<word>lamb</word><word>little</word><word>Mary</word>" + "</sentence>";

        String inputFile = "test.in";
        Scanner sc = Parser.getScannerForFile(inputFile);
        String in = "Mary had a little lamb";
        Sentence sentence = Sentence.buildSentence(in);

        in = sentence.printAsXMLSentenceWithWordsElements(SentenceService.sortLexicographically(sentence
                .getWordsInSentence()));

        in = StringUtils.deleteWhitespace(in);
        out = StringUtils.deleteWhitespace(out);

        Assert.assertEquals(in, out);

        LOGGER.info("We are parsing the data for file " + inputFile);

    }


    public void sentenceWordArraySortingTest() {
        List<String> words = Arrays.asList("a", "1", "z", "999999");
        SentenceService.sortLexicographically(words);
        Assert.assertEquals("1", words.get(0));
        Assert.assertEquals("999999", words.get(1));
        Assert.assertEquals("a", words.get(2));
        Assert.assertEquals("z", words.get(3));

    }
}
